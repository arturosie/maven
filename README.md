# Manual básico de Maven

Explicación básica del uso de Maven para la creación de proyectos Java, así como su configuración básica y documentación.

## Creación de proyecto Maven

Para crear un proyecto de Maven por terminal ejecutaremos el siguiente comando:

```cmd
mvn archetype:generate -DgroupId=com.mycompany.app -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false
```

Donde:

* **archetype:generate** - Indica que quieres crear un proyecto Maven.

* **-DgroupId** - Identifica contra todos los proyectos a través de un esquema de nombres. Es conveniente tener el sentido inverso del proyecto com.google.example.

* **-DartifactId** - Nombre del compilado o del proyecto.

* **-DarchetypeArtifactId** - Indica la estructura del proyecto.

* **-DarchetypeVersion** - Indica la versión.

* **-DartifactId** - Nombre del compilado o del proyecto.

* **-DinteractiveMode** - Asume opciones de configuración por defecto.

## Comandos
* **mvn compile** - Compila el proyecto en una carpeta que se llama target.
* **mvn package** - Empaqueta todo el proyecto y genera el compilado definido (por defecto jar)
mvn clean - Borra la carpeta target y su contenido.
* **mvn install** - Instalar dependencias de forma local para que maven lo reconozca en otros proyectos.

* **mvn eclipse:clean** - Borra todo lo que añade el ID Eclipse y no es propio de Maven.

## Creación proyecto Web

Para crear un proyecto web con Tomcat realizamos el mismo comando de creación de proyecto indicando lo siguiente:

```cmd
mvn archetype:generate -DgroupId=com -DartifactId=mavenWeb -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false
```

Como vemos, a cambiado ```-DarchetypeArtifactId=maven-archetype-webapp``` para indicar que es un proyecto web.

Instalamos Apache Tomcat. [Descarga aquí](http://tomcat.apache.org/).

Ejecutamos el comando ```mvn install```.

Entramos en la carpeta target y copiamos la carpeta del proyecto, en mi caso se llama mavenWeb.

Copiamos dicha carpeta dentro de Apache Tomcat (donde lo hayas instalado).

Ejecutamos Apache Tomcat.

Desde un navegador entramos para ver los resultados a localhost:[puerto], por defecto es el 8080. ```localhost:8080```


## Perfiles (Properties)

Indicamos la versión de maven a usar en el pom.xml.

```xml
<properties>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
</properties>
```

Donde:
* **maven.compiler.source** - La versión de java que se va a usar. Siempre inferior o igual al target.

* **maven.compiler.target** - Viene siendo el motor de jvm que se va a usar.

## Multiples POM
Con esto puedes usar un archivo pom en específico si tu proyecto tiene varios.

**En Eclipse:** 
```
-f [nombre del archivo pom]
```
**Terminal:**
```
mvn -f [nombre del archivo pom]
```

## Dependencias Transitivas
Son aquellas dependencias que necesitan de otras para ser utilizadas.

## Excluir dependencias
Para exlcuir una dependencia por incompatibilidad en nuestro proyecto podemos añadir lo siguiente en nuestro archivo pom:

```xml
<dependencies>
	<dependency>
		<exlcusions>
			<exclusion>
				<groupId>[nombre]</groupId>
				<artifactId>[nombre]</artifactId>
			</exclusion>
		</exlcusions>
	</dependency>
</dependencies>
```

## Documentación Maven
Agregamos en el pom un plugin para crear la documentación.
```xml
<build>
  	<plugins>
  		<plugin>
  			<groupId>org.apache.maven.plugins</groupId>
  			<artifactId>maven-site-plugin</artifactId>
  			<version>3.7.1</version>
  		</plugin>
  	</plugins>
</build>
```

Ejecutamos el comando en la terminal:
```cmd
mvn site
```

En la carpeta target/site se habrá creado un conjunto de archivos con formato de página web, si abrimos el index.html podremos ver la documentación

### Documentación en multi-idoma
Añadimos en el plugin del pom lo siguiente:
```xml
<configuration>
    <locales>en,fr</locales>
</configuration>
```
La documentación estara en ingles y frances.


### Documentación de Java (JavaDoc)

Añadimos en el pom lo siguiente:
```xml
<reporting>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-javadoc-plugin</artifactId>
            <version>3.2.0</version>
        </plugin>
    </plugins>
</reporting>
```

Ejecutamos el comando ```mvn site``` y aparecera en el index de la documentación.